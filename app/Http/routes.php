<?php

  use App\Message;
  use Illuminate\Http\Request;

  /**
   * get all messages
   */
  Route::get('/', function () {
  $msgs = Message::paginate(10);//show 10 per page

  return view('messages', [
    'msgs' => $msgs
  ]);
});

  /**
   * add message
   */
  Route::post('/add', function (Request $request) {
      
      $task = new Message;
      $task->name = $request->name;
      $task->email = $request->email;
      $task->homepage = $request->homepage;
      $task->message = $request->message;
      $task->save();//save new message
      
      //if file exist check it's resolution 
      //and if it's higher then 320/240 call resize function.
      
      
      if ($request->hasFile('image')) {
          $imageName = $task->id . '.' . $request->file('image')->getClientOriginalExtension();
          $request->file('image')->move( base_path() .'\public\images\catalog',$imageName);
          $path =  base_path() .'\public\images\catalog\\'.$imageName;
          list($oldwidth, $oldheight, $type) = getimagesize($path);
          if ($oldwidth>320 or $oldheight > 240){
              image_resize($path,$path,320,240);}
          }
      return redirect('/');
});

//show one message
  Route::get('/message/{id}', function ($id) {
      $msg = Message::find($id);
      return view('message',[
          'msg'=>$msg
      ]);
      
  }
);
// check if email exist for ajax
  Route::get('/email',function(Request $request){
      $email =$request->email;
      if (Message::where('email','=',$email)->exists()){
              return 'true';
      }
      else {
          return 'false';
      }
  });
  
//
function image_resize(
    $source_path, 
    $destination_path, 
    $newwidth,
    $newheight = FALSE, 
    $quality = FALSE // качество для формата jpeg
    ) {

    ini_set("gd.jpeg_ignore_warning", 1); // иначе на некотоых jpeg-файлах не работает
    
    list($oldwidth, $oldheight, $type) = getimagesize($source_path);
    
    switch ($type) {
        case IMAGETYPE_JPEG: $typestr = 'jpeg'; break;
        case IMAGETYPE_GIF: $typestr = 'gif' ;break;
        case IMAGETYPE_PNG: $typestr = 'png'; break;
    }
    $function = "imagecreatefrom$typestr";
    $src_resource = $function($source_path);
    
    if (!$newheight) { $newheight = round($newwidth * $oldheight/$oldwidth); }
    elseif (!$newwidth) { $newwidth = round($newheight * $oldwidth/$oldheight); }
    $destination_resource = imagecreatetruecolor($newwidth,$newheight);
    
    imagecopyresampled($destination_resource, $src_resource, 0, 0, 0, 0, 
            $newwidth, $newheight, $oldwidth, $oldheight);
    
    if ($type = 2) { # jpeg
        imageinterlace($destination_resource, 1); // чересстрочное формирование изображение
        imagejpeg($destination_resource, $destination_path, $quality);      
    }
    else { # gif, png
        $function = "image$typestr";
        $function($destination_resource, $destination_path);
    }
    
    imagedestroy($destination_resource);
    imagedestroy($src_resource);
} ;
