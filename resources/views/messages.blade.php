@extends('layouts.app')

@section('content')

    
    <div class="container">
        <div class="col-sm-offset-2 col-sm-8">
            <div class="panel panel-default">
                <div class="panel-heading">
                    New Message
                </div>
                <div class="panel-body">
                    <!-- Display Validation Errors -->
                    @include('common.errors')

                    <form enctype="multipart/form-data" action="{{ url('add')}}" method="POST" class="form-horizontal">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label  class="col-sm-3 control-label"></label>

                            <div class="col-sm-6">
                                <div> Your name<input autofocus=""type="text" name="name" id="task-name" class="form-control" required></div>
				<div id='divid'>Email<input type="email" name="email" id="email" class="form-control"></div>
                                <div>URL <input type="url" name="homepage" id="task-name" class="form-control" ></div>
                                <div>Message 
                                    <textarea  name="message" id="task-name" class="form-control" required></textarea>
                                </div>	
                                <div>
                                    <!-- accept only jpg, png and gif formats-->
                                    
                                    <input type="file" accept=".jpg,.png,.gif"name="image" >
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-6">
                                <button type="submit" class="btn btn-success" id="submitButton">
                                    <i class="fa fa-btn fa-plus"></i>Contact us
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

            @if (count($msgs) > 0)
            <!-- check if any messages exist. if yes then show message table -->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Messages
                    </div>

                    <div class="panel-body"><!-- use bootstrap sortable for table sorting-->
                        <table class="table table-bordered table-striped sortable">
                            <thead>
                                <th>date</th>
                                <th>name</th>
                                <th>email</th>
                                <th>homepage</th>
                                <th>message</th>
                            </thead>
                            <tbody>
                                @foreach ($msgs as $msg)
                                    <tr>
                                        <td>
                                            {{ date('h:i:s F d, Y ', strtotime($msg->created_at)) }}
                                        </td>
                                        <td><a href="{{ url('message',$msg->id)}}">{{ $msg->name }} </a></td>
                                        <td> {{ $msg->email }}</td>
                                        <td> {{ $msg->homepage }} </td>
                                        <!-- show only 5 chars of message-->
                                        <td>  {{  str_limit($msg->message,5) }}</td>
                                   </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            @endif
            
        </div>
        <!-- pagination-->
        <div class ='col-md-6'>{{ $msgs->links() }}</div>
        </div>

    </div>
@endsection