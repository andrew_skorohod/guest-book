@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="col-sm-offset-2 col-sm-8">
            <div class="panel panel-default">
                <div class="panel-body">
                    <p>Date: {{ date('h:i:s F d, Y ', strtotime($msg->created_at)) }}</p>
                    <p>UserName: {{ $msg->name }}</p>
                    <p>UserEmail:  {{ $msg->email }}</p>
                    <p>UserHomepage: {{ $msg->homepage }}</p>
                    <p>Message: {{  $msg->message }}</p>
                </div>
            </div>
        </div>
    </div>


@endsection