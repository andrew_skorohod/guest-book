
//this function uses ajax to check if email have been used already
$( "#email" ).change(function() {
  var email = $( this ).val();
  var data = {email:email};
  $.ajax({
	data:data,
	url: $('#sneaky_link').attr('href'),
	type:'GET',
	success:function(responce) {
            if (responce === 'true'){
                $( "#submitButton").attr('disabled',true);//if used disable submit and add messege
                var foo = '<p class="bg-danger">This email have been used already!</p>';
                $('#divid').append(foo);
	    }
            else { //remove disabled attr and message
                $( "#submitButton").attr('disabled',false);
                $('.bg-danger').remove();
            }
            }
	});
});

